export const state = () => {
    return {
        userName: "test"
    };
};

export const getters = {
};

export const mutations = {
    setUserName(state, payload) {
        state.userName = payload;
    }
};

export const actions = {

};
