/* eslint-disable no-unused-vars */
import jwtDecode from 'jwt-decode';
import Cookie from 'js-cookie';
export const state = () => {
    return {
        isUserLoggedIn: false, //是否登入
        userPicture: "", //會員照片
        userName: "", //會員名稱
        userUid: "", //會員 firebase 的 uid
        courses: [],
        userFavorite: null
    };
};

export const getters = {
    get_courses: state => {
        return state.courses.map(it => it.name);
    },
};

export const mutations = {
    set_courses: (state, payload) => {
        state.courses = payload;
    },
    setUserLoggedIn: (state, payload) => {
        state.isUserLoggedIn = true;
        state.userPicture = payload.userPicture || "https://bulma.io/images/placeholders/128x128.png";
        state.userName = payload.userName;
        state.userUid = payload.userUid;

        if (process.client) {
            Cookie.set("id_token", payload.id_token);
            Cookie.set("refresh_token", payload.refresh_token);
            Cookie.set('userUid', state.userUid);
            Cookie.set('userPicture', state.userPicture);
            Cookie.set('userName', state.userName);
        }

    },
    setUserlogout: (state) => {
        state.isUserLoggedIn = false;
        state.userPicture = "";
        state.userName = "";
        Cookie.remove('id_token');
        Cookie.remove('refresh_token');
        Cookie.remove('userUids');
        Cookie.remove('userPicture');
        Cookie.remove('userName');
        // router導向首頁
        $nuxt.$router.push({ name: 'index' });
    },
    set_userFavorite: (state, payload) => {
        state.userFavorite = payload || {};
    }
};

export const actions = {
    nuxtServerInit({ commit, dispatch }, context) {

        //這邊是給 Oauth 回來時提早觸發
        if (context.query.id_token && context.query.refresh_token) {
            let id_token_Decode = jwtDecode(context.query.id_token);
            commit('setUserLoggedIn', {
                id_token: context.query.id_token,
                refresh_token: context.query.refresh_token,
                userUid: id_token_Decode.user_id,
                userPicture: id_token_Decode.picture,
                userName: id_token_Decode.name,
            });

            context.app.$cookies.set("id_token", context.query.id_token);
            context.app.$cookies.set("refresh_token", context.query.refresh_token);
            context.app.$cookies.set("userUid", id_token_Decode.user_id);
            context.app.$cookies.set("userPicture", id_token_Decode.picture);
            context.app.$cookies.set("userName", id_token_Decode.name);

            //oauth回來時
            dispatch("saveMemberInfo", {
                idToken: context.query.id_token,
                name: id_token_Decode.name,
                picture: id_token_Decode.picture
            });
            return;
        }

        // 這邊是給重整時觸發,
        // 因為cookie會自動帶入http中,
        // 所以重整訪問時可利用此機制, 
        // 預先在server處理cookie資料
        if (context.app.$cookies.get('id_token')) {
            let picture = context.app.$cookies.get('userPicture');
            let name = context.app.$cookies.get('userName');
            let uid = context.app.$cookies.get('userUid');
            commit('setUserLoggedIn', {
                userUid: uid,
                userPicture: picture,
                userName: name,
            });
        }
    },
    getCoursesList({ commit }) {
        return this.$axios({
            method: this.app.$API.getCoursesList.method,
            url: this.app.$API.getCoursesList.url
        })
            .then(response => {
                let res = Object.keys(response.data)
                    .map(key => {
                        return {
                            id: key,
                            ...response.data[key]
                        };
                    })
                    .sort((it, it2) => it.order - it2.order);
                commit("set_courses", res);
            })
            .catch(error => {
                console.error(error);
            });
    },
    saveMemberInfo({ state }, payload) {
        let uid = (payload && payload.userUid) || state.userUid;
        let idToken = payload.idToken;
        delete payload.idToken;
        delete payload.uid;
        return this.$axios({
            method: this.app.$API.patchMemberInfo.method,
            url: this.app.$API.patchMemberInfo.url.replace(":user_id.json", uid + ".json") + "?auth=" + idToken,
            data: payload
        }).then((response) => {
            // console.log(response.data, "patchMemberInfo response");
        }).catch(error => {
            console.log(error, "error");
        });
    },
    getUserFavorite({ state, commit }) {
        if (!state.isUserLoggedIn) return;
        let uid = state.userUid;
        return this.$axios({
            method: this.app.$API.getMemberInfo.method,
            url: this.app.$API.getMemberInfo.url.replace(":user_id.json", uid + ".json") + "?auth=" + this.$cookies.get('id_token')
        }).then((response) => {
            console.error("getUserFavorite then");
            commit("set_userFavorite", response.data.favorite);
        }).catch(error => {
            console.error("getUserFavorite error:", error);
        });
    },
    updateUserFavorite({ state }) {
        return this.$axios({
            method: this.app.$API.patchMemberInfo.method,
            url: this.app.$API.patchMemberInfo.url.replace(":user_id.json", state.userUid + ".json") + "?auth=" + this.$cookies.get('id_token'),
            data: {
                favorite: state.userFavorite
            }
        }).then((response) => {
            console.log(response.data);
        }).catch(error => {
            console.log(error);
        });
    },
    exchangeToken({ state, commit }) {
        return new Promise((resolve, reject) => {
            this.$axios({
                method: this.app.$API.member.exchangeToken.method,
                url: this.app.$API.member.exchangeToken.url,
                baseURL: this.app.$config.google_api_url,
                data: this.app.$qs.stringify({
                    grant_type: 'refresh_token',
                    refresh_token: this.$cookies.get('refresh_token')
                }),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).then(res => {
                if (res) {
                    commit("setUserLoggedIn", {
                        id_token: res.data.id_token,
                        refresh_token: res.data.refresh_token,
                        userUid: state.userUid,
                        userPicture: state.userPicture,
                        userName: state.userName,
                    });
                    resolve(res);
                } else {
                    //refresh token失效
                    this.app.$Message({
                        message: '登入失效請重新登入',
                        type: 'error'
                    });
                    commit("setUserlogout");
                    reject(res);
                }
            });
        });
    }
};


