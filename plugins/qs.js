import Vue from 'vue';
import Qs from 'qs';

Vue.prototype.$qs = Qs;

export default ({ app }) => {
    // Set the function directly on the context.app object
    app.$qs = Qs;
};