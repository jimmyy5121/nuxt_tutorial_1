import API from "../API.js";
import Vue from "vue";

//註冊$API全局變量
Vue.use({
    install(Vue) {
        Vue.prototype.$API = API;
    }
});

//將全局變數注入到context.app中
export default ({ app }) => {
    // Set the function directly on the context.app object
    app.$API = API;
};