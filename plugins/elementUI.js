import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import { Message, Notification } from 'element-ui';
import Vue from 'vue';

Vue.use(ElementUI);

Vue.prototype.$Message = Message;
Vue.prototype.$Notify = Notification;

//將全局變數注入到context.app中
export default ({ app }) => {
    // Set the function directly on the context.app object
    app.$Message = Message;
    app.$Notify = Notification;
};