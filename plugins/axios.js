/* eslint-disable no-unused-vars */
export default function ({ $axios, redirect, error, $config, ...other }) {

    // console.log("other:", other);

    //也可以用這種方式設定統一設定header token
    //$axios.setHeader('Authorization', '123');

    //預設的 api 路徑
    $axios.setBaseURL($config.api_url);

    //每次請求時全域觸發
    $axios.onRequest(config => {
        // 每次請求傳送 firebase 的 key
        config.params = {
            key: $config.firebaseApiKey
        };
    });

    $axios.onResponse(config => {
        // console.log(config, "config");
    });

    $axios.onError(async data => {
        console.error("onError url:", data.config.url);
        console.error("onError config:", data.config.data);
        console.error("onError data:", data);
        const code = parseInt(data.response && data.response.status);
        if (code === 401) {
            return await other.app.store.dispatch("exchangeToken");
        }
        if (code === 500) {
            // Message({
            //     message: '警告哦，这是一条警告消息',
            //     type: 'error'
            // });
            //將錯誤資訊 帶到error.vue頁面中
            error({
                statusCode: code,
                message: {
                    responseData: data.response.data,
                    cool: '123'
                }
            });
        }
    });
}

