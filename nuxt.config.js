import webpack from 'webpack';
require('dotenv').config({ path: require('path').join(__dirname, `.env.${process.env.NODE_ENV}`) });
console.log("nuxt.config.js NODE_ENV", process.env.NODE_ENV);
console.log("nuxt.config.js web_url", process.env.web_url);
console.log("nuxt.config.js api_url", process.env.api_url);
console.log("nuxt.config.js google_api_url", process.env.google_api_url);
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Jim第一個網站",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    script: [
      {
        src:
          "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js"
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["@/assets/scss/main.scss", "normalize.css/normalize.css"],
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  // plugins 資料夾底下的JS,可用來限制說此套件只用於前端執行
  plugins: [
    { src: "~/plugins/datepicker.js", mode: "client" },
    { src: "~/plugins/gsap.js", mode: "client" },
    { src: '~/plugins/qs.js' },
    { src: '~/plugins/axios.js' },
    { src: '~/plugins/elementUI.js' },
    { src: '~/plugins/API.js' },
  ],
  loading: { color: '#40659b', height: "2px", duration: 5000 },

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "@nuxtjs/axios",
    "@nuxtjs/proxy",
    'cookie-universal-nuxt',
  ],
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: true,
    loaders: {
      imgUrl: { limit: 10000 } // 圖片大小設定小於多少byte時,會走base64編碼
    },
    plugins: [
      new webpack.ProvidePlugin({
        _CON: "~/constants.js",
      })
    ]

  },
  // 全局頁面切換動畫
  transition: {
    name: 'layout',
    mode: 'out-in'
  },
  // 另外起一個Node server服務
  serverMiddleware: [
    // 沒用到
    // { path: '/api', handler: '~/server/api.js' },
    { path: '/auth', handler: '~/server/auth.js' },
  ],
  // 前端處理CORS Proxy設定
  proxy: {
    '/mock': {
      target: 'http://localhost:3034/api',
      pathRewrite: {
        '^/mock': '/'
      }
    }
  },
  // env: {
  //   firebaseApiKey: "AIzaSyBeOMHe6J8tC3utAlfTpH2rBQ90kYQLPLM"
  // },
  publicRuntimeConfig: {
    NODE_ENV: process.env.NODE_ENV,
    web_url: process.env.web_url,
    firebaseApiKey: process.env.firebaseApiKey,
    google_api_url: process.env.google_api_url,
    api_url: process.env.api_url,
  },
  // 目前不會用到，只有在server端才能使用，client存取不到
  privateRuntimeConfig: {}
};
