const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

// middleware 允許CORS
app.use("/*", (req, res, next) => {
    // CORS處理
    res.header("Access-Control-Allow-Origin", "*");
    // HEAD,GET,POST都是簡單請求,其餘的非簡單請求
    // 若不加這行的話,嘗試用 Patch 等來請求,則會請求失敗
    res.header(
        "Access-Control-Allow-Methods",
        "GET,HEAD,OPTIONS,POST,PUT,DELETE,PATCH"
    );
    //如果前端有添加自定義headers時,加到第二個參數後面
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, AuthToken"
    );

    next();
});

app.use(express.json());  //接收 json 資料
app.use(express.urlencoded({ extended: false })); //接收 form urlencoded 的資料

const jim_nuxt = require("./routers/jim_nuxt");
app.use('/', jim_nuxt);

// middleware
app.use((req, res, next) => {
    console.log('Time:', Date.now());
    next();
});

app.use('/api', require("./routers/extend"));

app.get('/', (req, res) => res.send('Hello World!'));
app.listen(port, () => console.log(`mock server listening at http://localhost:${port}`));
