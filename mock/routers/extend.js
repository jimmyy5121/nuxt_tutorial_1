const express = require('express');
const multer = require("multer");
const fs = require("fs");
const app = express();
const Mock = require('mockjs');

app.get('/test', (req, res) => {
    res.json({
        title: 'this is mock data',
        message: 'Hello there!',
        //{"list":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6}]}}
        data: Mock.mock({
            // 属性 list 的值是一个数组，其中含有 1 到 10 个元素
            'list|1-10': [{
                // 属性 id 是一个自增数，起始值为 1，每次增 1
                'id|+1': 1
            }]
        }),
        email: Mock.Random.email('gmail.com'),
        ip: Mock.Random.ip()
    });
});

app.get('/courses', (req, res) => {
    res.json({
        courses: [{
            id: 1,
            name: "進入 python 的魔法世界 - 第一次學程式入門課",
            color: "#b5b5ac",
            img: "https://bulma.io/images/placeholders/1280x960.png",
            description: "這堂課使用python turtle 模組來創造幾何畫作，課程使用繪圖指令來講述程式語言的重要觀念，讓你打好程式語言的基礎。",
            introduction: "",
        }, {
            id: 2,
            name: "基礎教學資訊科技基礎教學",
            color: "#b5b5ac",
            img: "https://bulma.io/images/placeholders/1280x960.png",
            description: "這堂課教導基礎教學資訊科技基礎教學。",
            introduction: ""
        }, {
            id: 3,
            name: "illustrator基礎教學",
            color: "#f52e36",
            img: "https://bulma.io/images/placeholders/1280x960.png",
            description: "這堂課教導illustrator。",
            introduction: ""
        }]

    });
});

// Form表單
app.post('/form', (req, res) => {
    console.log("req body:", req.body);
    res.json({ result: 0 });
});

// 上傳檔案API
app.post("/file", async (req, res) => {
    let dir = "uploads";
    if (!fs.existsSync(dir)) fs.mkdirSync(dir);
    var storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, "./" + dir + "/");
        },
        filename: (req, file, cb) => {
            var name = file.originalname;
            var ext = name.split(".")[name.split(".").length - 1];
            cb(null, Date.now() + "." + ext);
        }
    });
    var upload = multer({ storage: storage }).single("videoFile");

    upload(req, res, () => {
        //回傳值
        res.status(200);
        res.json({
            code: 200,
            message: "上傳成功"
        });
    });
});

module.exports = app;

