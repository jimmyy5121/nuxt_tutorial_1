// module.exports = {
//   apps: [{
//     script: 'index.js',
//     watch: '.'
//   }, {
//     script: './service-worker/',
//     watch: ['./service-worker']
//   }],

//   deploy: {
//     production: {
//       user: 'SSH_USERNAME',
//       host: 'SSH_HOSTMACHINE',
//       ref: 'origin/master',
//       repo: 'GIT_REPOSITORY',
//       path: 'DESTINATION_PATH',
//       'pre-deploy-local': '',
//       'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production',
//       'pre-setup': ''
//     }
//   }
// }

module.exports = {
  apps: [{
    name: 'jim_nuxt_prod',
    script: './node_modules/nuxt/bin/nuxt.js',
    instances: '1', //負載平衡模式下的 cpu 數量
    exec_mode: "cluster", //cpu 負載平衡模式
    max_memory_restart: '1G', //緩存了多少記憶體重新整理
    port: 3001, //指定伺服器上的 port
    time: true,
    env: {
      NODE_ENV: "prod"
    }
  },
  {
    name: 'jim_nuxt_sit',
    script: './node_modules/nuxt/bin/nuxt.js',
    instances: '1', //負載平衡模式下的 cpu 數量
    exec_mode: "cluster", //cpu 負載平衡模式
    max_memory_restart: '1G', //緩存了多少記憶體重新整理
    port: 3002, //指定伺服器上的 port
    env: {
      NODE_ENV: "sit"
    }
  }
  ],
  deploy: {
    "prod": {
      "user": "jimmyy5121", //linux 登入帳號 帳號@ip
      "host": ["185.201.8.225"], //你的伺服器 ip
      // "key": "c:/Users/jimmy/.ssh/GCP_NO_PWD", //指定ssh
      "ref": "origin/main", //分支
      "repo": "git@gitlab.com:jimmyy5121/nuxt_tutorial_1.git", //ssh 的 git
      "path": "/home/root/nuxt_tutorial_1", //伺服器上的路徑
      "post-deploy":
        "npm install && npm run build && pm2 reload ecosystem.config.js --only jim_nuxt_prod", //佈署指令
    }
  }
};
