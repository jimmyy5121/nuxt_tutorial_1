/* eslint-disable no-undef */
// import { mount, shallowMount } from '@vue/test-utils';
import { mount } from '@vue/test-utils';
import ClickButton from "@/components/click-button";

describe("測試 ClickButton", () => {
    const wrapper = mount(ClickButton, {
        propsData: {
            propsCount: 10
        }
    });
    const button = wrapper.find('button');
    const span = wrapper.find('span');

    console.log(wrapper.html());
    //檢查某個元素是否存在
    it('has a button', () => {
        expect(button.exists()).toBe(true);
    });

    // 檢查指定html內容
    it('has a span1', () => {
        expect(wrapper.html()).toContain(`<span class="count">total count:10</span>`);
    });

    //檢查指定html文本
    it('has a span2', () => {
        expect(span.text()).toContain(`total count:10`);
    });

    //模擬按鈕點擊,並檢測數據有無變動
    it('button click should increment the count', () => {
        expect(wrapper.vm.count).toBe(10);
        const button = wrapper.find('button');
        button.trigger('click');
        expect(wrapper.vm.count).toBe(11);
    });

    //模擬按鈕點擊,並檢測組件DOM有沒有確實寫入
    it('button click should increment the count text', async () => {
        expect(span.text()).toContain('total count:11');
        const button = wrapper.find('button');
        await button.trigger('click');
        expect(span.text()).toContain('total count:12');
    });
});